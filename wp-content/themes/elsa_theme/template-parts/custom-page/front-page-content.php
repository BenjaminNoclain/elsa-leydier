<?php 

$args = [
        'post_type'      => 'front-content',
        'orderby'          => 'date',
        'order'            => 'DESC',
    ];

$fronts = get_posts($args);

$front = $fronts[0];

?>
<div class="main-content offset-lg-3 col-lg-9 offset-xl-2 col-xl-10"> 
    <?php echo get_post_field('post_content', $front->ID); ?>
</div>