<?php

$layout = get_field('artwork_serie_layout_type');

get_header(); ?>
<div id="primary">
    <main id="main" class="site-main" role="main">
        <div class="container-fluid">
            <div class="row">
                <?php get_template_part('template-parts/nav'); ?>
                <div class="main-content offset-lg-3 col-lg-9 offset-xl-2 col-xl-10">
                    <h1 class="title-serie"><?php echo get_the_title(); ?></h1>
                    <p><?php echo get_field('artwork_serie_description') ?></p>

                    <?php if ($layout == 'horizontaly') {

                        $posts = get_field('artwork_serie_medias');
                        if ($posts && count($posts) > 1) : 
                        
                        var_dump(count($posts));
                        ?>
                            <div class="owl-carousel owl-theme owl-one">
                           <?php foreach ($posts as $post) : // variable must be called $post (IMPORTANT) 
                    ?>
                                <?php setup_postdata($post);
                                $attachment = get_post($post);
                                $meta = array(
                                    'alt' => get_post_meta($attachment->ID, '_wp_attachment_image_alt', true),
                                    'caption' => $attachment->post_excerpt,
                                    'description' => $attachment->post_content,
                                    'href' => get_permalink($attachment->ID),
                                    'src' => $attachment->guid,
                                    'title' => $attachment->post_title
                                );
                                $showInfo = get_field('field_5d333802d85ed', $attachment->ID);
                                ?>
                                
                                    <div class="item">
                                        <div class="artwork-serie">
                                            <a href="<?php the_permalink(); ?>" rel="prettyPhoto[gallery_name]">
                                                <img src="<?php echo wp_get_attachment_url(); ?>" />
                                                <?php if ($showInfo == "Oui") : ?>
                                                    <div class="artwork-info">
                                                        <div class="container-skew">
                                                            <div class="skew"></div>
                                                        </div>
                                                        <p class="artwork-title"><?php echo get_the_title(); ?></p>
                                                        <p class="artwork-desc"><?php echo $meta['description']; ?></p>
                                                    </div>
                                                <?php endif; ?>
                                            </a>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                                <div class="owl-navigation"></div>
                                <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly 
                                ?>
                                <?php else : 
                                
                                    setup_postdata($posts[0]);
                                    $attachment = get_post($posts[0]);
                                    $meta = array(
                                        'alt' => get_post_meta($attachment->ID, '_wp_attachment_image_alt', true),
                                        'caption' => $attachment->post_excerpt,
                                        'description' => $attachment->post_content,
                                        'href' => get_permalink($attachment->ID),
                                        'src' => $attachment->guid,
                                        'title' => $attachment->post_title
                                    );
                                    $showInfo = get_field('field_5d333802d85ed', $attachment->ID);
                                    ?>
                                    <div class="item d-flex justify-content-center">
                                        <div class="artwork-serie">
                                            <a href="<?php echo $meta['href'] ?>" rel="prettyPhoto[gallery_name]">
                                                <img src="<?php echo $meta['src'] ?>" />
                                                <?php if ($showInfo == "Oui") : ?>
                                                    <div class="artwork-info">
                                                        <div class="container-skew">
                                                            <div class="skew"></div>
                                                        </div>
                                                        <p class="artwork-title"><?php echo $meta['title'] ?></p>
                                                        <p class="artwork-desc"><?php echo $meta['description']; ?></p>
                                                    </div>
                                                <?php endif; ?>
                                            </a>
                                        </div>
                                    </div>
                            <?php endif; ?>
                                </div>
                            <?php } ?>
                            <?php if ($layout == 'verticaly') { ?>
                                <section id="grid-container" class="grid transitions-enabled fluid masonry js-masonry grid">
                                    <?php

                                    $posts = get_field('artwork_serie_medias');

                                    /*
                $ids = get_field('artwork_serie_medias', false, false);
                $args = array(
                'post__in' => $ids,
                'orderby' =>  'post__in' 
                );

                $posts = new WP_Query( $args);
                var_dump($posts);
                die;*/
                                    if ($posts) :

                                        foreach ($posts as $post) : // variable must be called $post (IMPORTANT) 
                                    ?>
                                            <?php setup_postdata($post);
                                            $attachment = get_post($post);
                                            $meta = array(
                                                'alt' => get_post_meta($attachment->ID, '_wp_attachment_image_alt', true),
                                                'caption' => $attachment->post_excerpt,
                                                'description' => $attachment->post_content,
                                                'href' => get_permalink($attachment->ID),
                                                'src' => $attachment->guid,
                                                'title' => $attachment->post_title
                                            );
                                            $showInfo = get_field('field_5d333802d85ed', $attachment->ID);

                                            ?>
                                            <div class="grid-item gutter-sizer">
                                                <div class="artwork-serie">
                                                    <a href="<?php the_permalink(); ?>" rel="prettyPhoto[gallery_name]">
                                                        <img src="<?php echo wp_get_attachment_url(); ?>" />
                                                        <?php if ($showInfo == "Oui") : ?>
                                                            <div class="artwork-info">
                                                                <div class="container-skew">
                                                                    <div class="skew"></div>
                                                                </div>
                                                                <p class="artwork-title"><?php echo get_the_title(); ?></p>
                                                                <p class="artwork-desc"><?php echo $meta['description']; ?></p>
                                                            </div>
                                                        <?php endif; ?>
                                                    </a>
                                                </div>
                                            </div>

                                        <?php endforeach; ?>

                                        <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly 
                                        ?>
                                    <?php endif; ?>
                                </section>
                            <?php } ?>
                </div>
            </div>
        </div>
    </main><!-- #main -->
</div><!-- #primary -->
<?php

get_footer(); ?>