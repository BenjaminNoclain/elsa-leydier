(function( $ ) {
  $(document).ready(function(){

$("a[rel^='prettyPhoto']").prettyPhoto({
  
});
 //a l'affiche
    var carousel = $(".owl-one");
        carousel.owlCarousel({ 
        dots:true,    
        loop:false,
        autoWidth:true,
        margin:10,
        autoHeight:true, 
		responsiveClass:true,
		 responsive:{
            0:{
              items:1
              },
            600:{
                  items:1
              },
            1000:{
                  items:2
              },
            1200:{
                  items:3
              }
            }
          });

      
    // init Masonry
    var $grid = $('.grid').masonry({
        // options
        itemSelector: '.grid-item',
        columnWidth: '.grid-item',
        gutter: '.gutter-sizer',
        horizontalOrder: true,
    });
    // layout Masonry after each image loads
    $grid.imagesLoaded().progress( function() {
        $grid.masonry('layout');
    });

    $("button#nav-icon4").on('click', function(event){
      
      var nav = $('nav');
      nav.toggleClass('open');
    })


    $(".menu-item > a").on('click', function(event){
      var element = $(this).parents('.menu-item').find('.sub-menu');
      var img = $(this).parent().find('svg');
      if(element.hasClass('d-none')){  
        img.toggleClass('flip');
        element.toggleClass('open');
        event.stopPropagation();
      } else {
        img.toggleClass('flip');
        element.toggleClass('open');
        event.stopPropagation();
      }
      
  });

  var svgArrow = document.getElementById("svg-arrow");
  $(".menu-item-has-children > a").append( svgArrow );
  
  $(".menu-item.active").parent().addClass('open');
  $(".current-menu-parent").find(".svg-menu svg").addClass('flip');
  
    $('nav #nav-icon4').on('click', function(event){
        var element = $(this).parent().find('ul');
        if(element.hasClass('d-none')){  
          $(this).toggleClass('open');    
          element.removeClass('d-none');
          event.stopPropagation();
        } else {
          $(this).toggleClass('open');     
          element.addClass('d-none');
          event.stopPropagation();
        }
    });

  });

})( jQuery );
