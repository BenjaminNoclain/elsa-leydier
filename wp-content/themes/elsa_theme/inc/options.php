<?php
class MySettingsPage
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        add_options_page(
            'Settings Admin', 
            'Contact & Social Network', 
            'manage_options', 
            'my-setting-admin', 
            array( $this, 'create_admin_page' )
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option( 'my_option_name' );
        ?>
        <div class="wrap">
            <h1>Contact & Social Network</h1>
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( 'my_option_group' );
                do_settings_sections( 'my-setting-admin' );
                submit_button();
            ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {        
        register_setting(
            'my_option_group', // Option group
            'my_option_name', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        add_settings_section(
            'setting_section_id', // ID
            'Link for footer', // Title
            array( $this, 'print_section_info' ), // Callback
            'my-setting-admin' // Page
        );  

        add_settings_field(
            'instagram', // ID
            'Instagram Link', // Title 
            array( $this, 'instagram_callback' ), // Callback
            'my-setting-admin', // Page
            'setting_section_id' // Section           
        );      

        add_settings_field(
            'mail', 
            'Contact Mail', 
            array( $this, 'mail_callback' ), 
            'my-setting-admin', 
            'setting_section_id'
        );  
        add_settings_field(
            'color', 
            'Color', 
            array( $this, 'color_callback' ), 
            'my-setting-admin', 
            'setting_section_id'
        );    
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $new_input = array();
        if( isset( $input['instagram'] ) )
            $new_input['instagram'] = sanitize_text_field( $input['instagram'] );

        if( isset( $input['mail'] ) )
            $new_input['mail'] = sanitize_text_field( $input['mail'] );
        
        if( isset( $input['color'] ) )
            $new_input['color'] = sanitize_text_field( $input['color'] );    

        return $new_input;
    }

    /** 
     * Print the Section text
     */
    public function print_section_info()
    {
        print 'Enter your settings below:';
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function instagram_callback()
    {
        printf(
            '<input type="text" id="instagram" name="my_option_name[instagram]" value="%s" />',
            isset( $this->options['instagram'] ) ? esc_attr( $this->options['instagram']) : ''
        );
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function mail_callback()
    {
        printf(
            '<input type="text" id="mail" name="my_option_name[mail]" value="%s" />',
            isset( $this->options['mail'] ) ? esc_attr( $this->options['mail']) : ''
        );
    }

        /** 
     * Get the settings option array and print one of its values
     */
    public function color_callback()
    {
        printf(
            '<input type="text" id="color" name="my_option_name[color]" value="%s" />',
            isset( $this->options['color'] ) ? esc_attr( $this->options['color']) : ''
        );
    }
}

if( is_admin() )
    $my_settings_page = new MySettingsPage();