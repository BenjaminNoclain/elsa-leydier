<?php 

function front_content_init() {
    $args = array(
      'label' => 'Front page content',
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => array('slug' => 'front-content'),
        'query_var' => true,
        'supports' => array( 'title', 'editor', 'author', 'revisions' ),
        'menu_icon' => 'dashicons-edit',
        );
    register_post_type( 'front-content', $args );
}
add_action( 'init', 'front_content_init' );

?>