<?php 

function artwork_serie_init() {
    $args = array(
      'label' => 'Artwork serie',
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => array('slug' => 'artwork-serie'),
        'query_var' => true,
        'supports' => array( 'title', 'author', 'thumbnail', 'revisions' ),
        'menu_icon' => 'dashicons-edit',
        );
    register_post_type( 'artwork_serie', $args );
}
add_action( 'init', 'artwork_serie_init' );

?>