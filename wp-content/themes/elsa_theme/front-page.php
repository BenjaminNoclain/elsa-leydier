<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <h1 class="test" >Test</h1>
            </div>
        </div>
	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
