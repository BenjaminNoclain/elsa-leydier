<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
$options = get_option('my_option_name');
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/template.css?v=1.4">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/prettyPhoto.css" type="text/css" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/prettyPhoto.minimal.css" type="text/css" />
<?php wp_head(); ?>
</head>
<style>
    .nav-item a:hover {
    color: <?=$options['color']?>;
    }
    .switch-lang select option {
    color: <?=$options['color']?>;
}
.switch-lang select option:hover {
    color: <?=$options['color']?>;
}
.switch-lang select {
    color: <?=$options['color']?>;
}
.nav-social a img {
    fill: <?=$options['color']?>;
}
#nav-icon4 span {
  background: <?=$options['color']?>;
}
.artwork-title, .artwork-desc {
    color: <?=$options['color']?>;
}
.owl-theme .owl-dots .owl-dot.active span, .owl-theme .owl-dots .owl-dot:hover span {
    background: <?=$options['color']?>!important;
}
div.pp_default .pp_details .pp_close {
    background: none;
    background-color: <?=$options['color']?>!important;
    -webkit-mask-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/multiply.svg);
    -webkit-mask-repeat: no-repeat;
    -webkit-mask-size: contain;
    mask-repeat: no-repeat;
}
div.pp_default .pp_next {
    background: none;
    background-color: <?=$options['color']?>!important;
    -webkit-mask-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/right.svg);
    -webkit-mask-repeat: no-repeat;
    width: 50px;
    height: 50px;
    -webkit-mask-size: contain;
    mask-repeat: no-repeat;
    position: relative;
    top: 50%;
    }

    div.pp_default .pp_previous {
    background: none;
    background-color: <?=$options['color']?>!important;
    -webkit-mask-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/left.svg);
    -webkit-mask-repeat: no-repeat;
    width: 50px;
    height: 50px;
    -webkit-mask-size: contain;
    mask-repeat: no-repeat;
    position: relative;
    top: 50%;
    }
.title-serie {
    border-bottom: 1px solid <?=$options['color']?>;
}    
.navigation-principale {
    background-image: linear-gradient(-130deg, <?=$options['color']?>10, transparent)
}
.submenu.open, ul.sub-menu.open a:hover{
    color: <?=$options['color']?>;
}
.submenu.open, ul.sub-menu.open li.active a{
    color: <?=$options['color']?>;
}
</style>    
<body>

