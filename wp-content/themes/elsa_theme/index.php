<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */


$args = [
        'post_type'      => 'front-content',
        'orderby'          => 'date',
        'order'            => 'DESC',
    ];

$fronts = get_posts($args);

$front = $fronts[0];

get_header(); ?>
<style>
@media screen and (max-width: 768px){ 
    .background-front {
        background-image:url('<?php echo get_field('front_page_image_mobile', $front->ID); ?> ');
    }
}

@media screen and (min-width: 768px){ 
    .background-front {
        background-image:url('<?php echo get_field('front_page_image_desktop', $front->ID); ?> ');
    }
    
}
</style>


<div id="primary" class="content-area background-front">
	<main id="main" class="site-main" role="main">
        <div class="container-fluid">
            <div class="row">
            
                <?php get_template_part( 'template-parts/nav' ); ?>
                <?php get_template_part( 'template-parts/custom-page/front-page-content' ); ?>
                
            </div>
        </div>
	</main><!-- #main -->
</div><!-- #primary -->
<?php
get_footer();
